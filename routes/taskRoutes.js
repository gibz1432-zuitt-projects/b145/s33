const express = require('express');
const router = express.Router();
const taskControllers = require('../controllers/taskControllers');

router.get('/', (req,res) => {
	taskControllers.getAllTasks().then(resultFromController => res.send(resultFromController));
});


//route for create task

router.post('/createTask', (req,res) => {
	taskControllers.createTask(req.body).then(resultFromController => res.send(resultFromController));
});



// Deleting

router.delete('/deleteTask/:id', (req,res) => {
	taskControllers.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))
})

//Updating task
router.put('/updateTask/:id', (req,res) => {
	taskControllers.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController))
})


//ACTIVITY

//Getting a specific task

router.get('/:id', (req,res) => {
	taskControllers.getTask(req.params.id).then(resultFromController => res.send(resultFromController))
})


//Updating a status of a task

router.put('/:id/:complete', (req,res) => {
	taskControllers.updateTaskStatus(req.params.id,req.params.complete).then(resultFromController => res.send(resultFromController))
})


module.exports = router;