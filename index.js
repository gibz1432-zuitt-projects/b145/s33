const express = require('express');
const mongoose = require('mongoose');
const taskRoutes = require('./routes/taskRoutes')
// const createTask = require('./routes/createTask')
const port = 4000;
const app = express();

mongoose.connect("mongodb+srv://orfenoko1:malnour12345@gibz.8x8pf.mongodb.net/session33?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;
db.on("error", console.error.bind(console, "There is an error with the connection"));
db.once("open", () => console.log("Successfully connected to the database"));

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use('/tasks', taskRoutes);



app.listen(port, () => console.log(`Server running at port ${port}`));